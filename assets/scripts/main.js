
$(document).ready(function() {

    CustomScroll();

    CollapseCards();
    $('.tabs-panel').find('.type-news').click(function(){
        var cond = $(this).attr('condition');
        switch(cond) {
            case 'expanded':
                var attr = 'collapsed';
                break;
            case 'collapsed':
                var attr = 'expanded';
                break;
            default:
                console.log('wrong condition');
                return;
        }
        newHeight = $(this).attr(attr+'-height');
        $(this).height(newHeight);
        $(this).attr('condition', attr);
        $(this).removeClass(attr=='expanded'?'collapsed':'expanded');
        $(this).addClass(attr);

        var offsetTop = $('.tabs').outerHeight();
        var old = $('.tabs-panel').scrollTop();
        var offset = $(this).position();
        $('.tabs-panel').animate({
            scrollTop: old + offset.top
        }, 500);

    });
});

function CollapseCards()
{
    $('.tabs-panel').find('.type-news').each(function(i)
    {
        var el = $(this);
        el.attr('expanded-height', el.height());
        var collapsedHeight = el.height() - el.find('.text').height() + 82;
        el.height(collapsedHeight);
        el.attr('collapsed-height', collapsedHeight);
        el.addClass('collapsed');
        el.attr('condition', 'collapsed');
    });
    $('.jspContainer').height('auto');
}

function CustomScroll(){
    baron({
        root: '.js-tabs-wrapper',
        scroller: '.js-tabs',
        bar: '.baron-bar',
        scrollingCls: 'tabs--scrolling',
        draggingCls: 'tabs--dragging'
    }).controls({
        track: '.baron-track'
    });
}

$( window ).resize(function(){

});
$(document).foundation();

